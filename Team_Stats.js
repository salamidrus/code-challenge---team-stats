const team = {
    _players: [
      {firstName: 'Pablo',
        lastName: 'Sanchez',
        age: 11},
      {firstName: 'Joao',
        lastName: 'Mario',
        age: 12},
      {firstName: 'Lucas',
        lastName: 'Piere',
        age: 13}
    ],
    _games: [
      {opponent: 'Broncos',
      teamPoints: 42,
      opponentPoints: 27},
      {opponent: 'Brontos',
      teamPoints: 40,
      opponentPoints: 25},
      {opponent: 'Loka',
      teamPoints: 45,
      opponentPoints: 50}
    ],
    
    get players() {
      return this._players;
    },
    
    get games() {
      return this._games;
    },
    
    addPlayer(firstName, lastName, age) {
      let player = {
        firstName: firstName,
        lastName: lastName,
        age: age,
      };
      this.players.push(player);
    },
    addGame(opp, myPts, oppPts) {
      const game = {
        opponent: opp,
        points: myPts,
        opponentPoints: oppPts
      };
      this.games.push(game);
    }
  }
  
  team.addPlayer('Steph', 'Curry', 28);
  team.addPlayer('Lisa', 'Leslie', 44);
  team.addPlayer('Bugs', 'Bunny', 76);
  
  console.log(team.players);
  
  team.addGame('Hago', 99, 87);
  team.addGame('Nona', 85, 100);
  team.addGame('lola', 86, 90);
  
  console.log(team.games);
  